import React from 'react';
import './Posts.css';
import {ListGroup, ListGroupItem} from "react-bootstrap";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import mapDispatchToProps from "react-redux/src/connect/mapDispatchToProps";
import mapStateToProps from "react-redux/src/connect/mapStateToProps";


class Posts extends React.Component {

  render () {
    return (
      <div className="container">
        <h4 className="posts">Posts will be here</h4>
        <Link to="addNewPost"> <button className="button" > Add new post </button></Link>


        <ListGroup >
          {this.props.news.map(news=> (
              <ListGroupItem key={news.id}>
                <h5 className="name">{news.title}</h5>
                <p>{news.content}</p>

              </ListGroupItem>
            )
          )}
        </ListGroup>
      </div>
    )
  }

}
export default connect(mapStateToProps, mapDispatchToProps)(Posts);
