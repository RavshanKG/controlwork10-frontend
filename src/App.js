import React, { Component } from 'react';
import './App.css';
import Header from "./components/Header/Header";
import {Route, Switch} from "react-router";
import Posts from "./containers/Posts/Posts";
import AddNewPost from "./components/AddNewPost/AddNewPost";
import ReadFullPost from "./components/ReadFullPost/ReadFullPost";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Switch>
          <Route path="/" exact component={Posts} />
          <Route path="/" exact component={ReadFullPost} />
          <Route path="/addNewPost" exact component={AddNewPost} />
        </Switch>
      </div>
    );
  }
}

export default App;

