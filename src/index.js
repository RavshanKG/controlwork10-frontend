import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {Provider} from "react-redux";
import {BrowserRouter} from "react-router-dom";
import {createStore, applyMiddleware, compose} from "redux";
import reducer from "./store/reducers/news";
import thunkMiddleware from 'redux-thunk';


const store = createStore(reducer,applyMiddleware(thunkMiddleware));


const Application = (
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
)

ReactDOM.render(Application, document.getElementById('root'));
registerServiceWorker();
