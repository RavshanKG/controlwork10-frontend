import {FETCH_NEWS_SUCCESS} from "./actionTypes";
import axios from '../../axios-api';

export const fetchNewsSuccess = news => {
  return {type: FETCH_NEWS_SUCCESS, news};
};

export const fetchNews = () => {
  return dispatch => {
    return axios.get('/news').then(
      response => dispatch(fetchNewsSuccess(response.data))
    );
  };
};