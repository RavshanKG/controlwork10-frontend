import React from 'react';
import {Navbar} from "react-bootstrap";
import {NavLink} from "react-router-dom";

const Header = () => {
  return (
    <Navbar inverse collapseOnSelect>
      <Navbar.Header>
        <Navbar.Brand>
          <NavLink to="/" exact> News</NavLink>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
    </Navbar>
  )
}
export default Header;