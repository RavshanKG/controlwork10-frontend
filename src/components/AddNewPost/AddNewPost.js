import React from 'react';
import {Button, ControlLabel, FormControl, FormGroup} from "react-bootstrap";
import {connect} from 'react-redux';
import {fetchNews} from "../../store/actions/News";
import './AddNewPost.css';

class AddNewPost extends React.Component {
  state = {
    title: '',
    content: '',
    img: ''
  };

  componentDidMount() {
    this.props.onFetchNews();
  }


  submitForm = event => {
    event.preventDefault();
    this.props.history.replace('/');
  };

  inputChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  render () {
    return (
      <div className="container">
        <h4 className="add">Add new post</h4>
        <form onSubmit={this.submitForm}>
          <FormGroup controlId="formControlsSelectMultiple">
            <ControlLabel className="title">Title</ControlLabel>
            <FormControl name="title" placeholder="Title" required value={this.state.title} onChange={this.inputChange} type="text">
            </FormControl>
          </FormGroup>
          <FormGroup controlId="formControlsTextarea">
            <ControlLabel className="content">Content</ControlLabel>
            <FormControl name="content" className="content-input" value={this.state.content} onChange={this.inputChange} componentClass="textarea" placeholder="Content" required />
          </FormGroup>
          <ControlLabel className="image">Image</ControlLabel>
          <input className="image-input" type="file" name="img" accept="image/*" value={this.state.img} onChange={this.inputChange} />


          <Button type={"submit"} >Submit</Button>
        </form>
      </div>
    )
  }
}



const mapStateToProps = state => {
  return {news: state.news};
};

const mapDispatchToProps = dispatch => {
  return {onFetchNews: () => dispatch(fetchNews())};
};

export default connect(mapStateToProps, mapDispatchToProps)(AddNewPost);

